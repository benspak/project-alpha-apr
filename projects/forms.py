from django import forms
from .models import Project
from django.contrib.auth.models import User


class ProjectForm(forms.ModelForm):
    owner = forms.ModelChoiceField(
        queryset=User.objects.all(), empty_label=None, widget=forms.Select
    )

    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
        widgets = {
            "name": forms.TextInput(attrs={"maxlength": 200}),
            "description": forms.Textarea(),
        }
